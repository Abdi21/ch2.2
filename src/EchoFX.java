import java.util.Scanner;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class EchoFX extends Application {
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Byte byteValue;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a byte value (number between -128 and 127");
		byteValue = keyboard.nextByte();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Echo App");
		alert.setContentText("You have just entered " + byteValue);
		alert.showAndWait();

	}

}
